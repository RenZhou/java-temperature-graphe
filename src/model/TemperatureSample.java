package model;

import java.util.Date;

public class TemperatureSample {
    /**
     * The value of the temperature
     */
    private final double readvalue;

    /**
     * The date of the sample
     */
    private final Date readDate;

    /**
     * The sample number (a identifier which represent the position of the sample)
     */
    private final int sampleNumber;


    /**
     * Constructor which take all needed parameters
     * @param readvalue
     * @param readDate
     * @param sampleNumber
     */
    public TemperatureSample(double readvalue, Date readDate, int sampleNumber) {
        this.readvalue = readvalue;
        this.readDate = readDate;
        this.sampleNumber = sampleNumber;
    }


    public double getValue() {
        return readvalue;
    }

    public Date getReadDate() {
        return readDate;
    }

    public int getSampleNumber() {
        return sampleNumber;
    }
}

