package model;

import java.util.ArrayList;
import java.util.List;

public class TemperatureClientModel {
    private final List<TemperatureSample> samples;

    public TemperatureClientModel() {
        this.samples = new ArrayList<TemperatureSample>();
    }

    public void addSample(TemperatureSample sample) {
        this.samples.add(sample);
    }

    public List<TemperatureSample> getSamples() {
        return samples;
    }

    public int size() {
        return samples.size();
    }
}