package model;

import org.math.plot.Plot2DPanel;
import server.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Temperature {

    private JFrame frame;
    private JTextField txtIP;
    private JTextField txtPort;
    private JButton Ok;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Temperature window = new Temperature();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Temperature() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.getContentPane().setForeground(Color.BLACK);
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        txtIP = new JTextField();
        txtIP.setForeground(Color.BLACK);
        txtIP.setText("127.0.0.1");
        txtIP.setBounds(30, 50, 100, 30);
        txtIP.setColumns(10);
        frame.getContentPane().add(txtIP);

        txtPort = new JTextField();
        txtPort.setForeground(Color.BLACK);
        txtPort.setText("5050");
        txtPort.setBounds(200, 50, 100, 30);
        txtPort.setColumns(10);
        frame.getContentPane().add(txtPort);


        Ok = new JButton();
        Ok.setText("Connect");
        Ok.setBounds(30, 100, 128, 30);
        frame.getContentPane().add(Ok);



        Ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String serverIP = txtIP.getText();
                String port = txtPort.getText();
                if (!serverIP.isEmpty() && !port.isEmpty()) {
                    Client client = new Client(serverIP, port);
                    for (int i = 0; i < 10; ++i) {
                        client.getTemperature();
                    }
                    lanceGraphe(client.getTCM());
                }
            }
        });
    }

    private void lanceGraphe(TemperatureClientModel tcm) {
        double[] x = new double[tcm.size()];
        double[] y = new double[tcm.size()];
        double[] min_point = new double[tcm.size()];
        double[] max_point = new double[tcm.size()];
        double[] average_point = new double[tcm.size()];
        String[] labelX = new String[tcm.size()];
        List<TemperatureSample> samples = tcm.getSamples();
        double somme = 0, min = samples.get(0).getValue(), max = 0;

        for (int i = 0; i < samples.size(); ++i) {
            x[i] = samples.get(i).getSampleNumber();
            y[i] = samples.get(i).getValue();
            System.out.println(x[i] + " " + y[i]);
            somme += y[i];
            if (y[i] > max) {
                max = y[i];
            }
            if (y[i] < min) {
                min = y[i];
            }
            labelX[i] = samples.get(i).getReadDate().toString();
        }
        double average = somme / (double) samples.size();
        for (int i = 0; i < samples.size(); ++i) {
            min_point[i] = min;
            max_point[i] = max;
            average_point[i] = average;
        }
        // create your PlotPanel (you can use it as a JPanel)
        Plot2DPanel plot = new Plot2DPanel();
        plot.setAxisLabels("<X>", "frequency");

        // add a line plot to the PlotPanel
        plot.addLinePlot("my plot", x, y);
        plot.addLinePlot("my plot", x, min_point);
        plot.addLinePlot("my plot", x, max_point);
        plot.addLinePlot("my plot", x, average_point);

        // put the PlotPanel in a JFrame, as a JPanel
        JFrame frame = new JFrame("a plot panel");
        frame.setBounds(100, 100, 450, 300);
        frame.setContentPane(plot);
        frame.setVisible(true);
    }

    private static void setVisible(boolean b) {
        // TODO Auto-generated method stub

    }
}
