package server;/* The Connection Handler Class - Written by Derek Molloy for the EE402 Module
 * See: ee402.eeng.dcu.ie
 */
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.io.BufferedReader;

public class ThreadedConnectionHandler extends Thread
{
    private Socket clientSocket = null;    // server.Client socket object
    private ObjectInputStream is = null;   // Input stream
    private ObjectOutputStream os = null;   // Output stream
    private DateTimeService theDateService;
    private static String TEMP_PATH2 = "/sys/class/thermal/thermal_zone0/temp";
    private double temperature = 0;


    // The constructor for the connection handler
    public ThreadedConnectionHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
        //Set up a service object to get the current date and time
        theDateService = new DateTimeService();
    }

    // Will eventually be the thread execution method - can't pass the exception back
    public void run() {
        try {
            this.is = new ObjectInputStream(clientSocket.getInputStream());
            this.os = new ObjectOutputStream(clientSocket.getOutputStream());
            while (this.readCommand()) {}
        }
        catch (IOException e)
        {
            System.out.println("XX. There was a problem with the Input/Output Communication:");
            e.printStackTrace();
        }
    }


    // Receive and process incoming string commands from client socket 
    private boolean readCommand() {
        String s = null;
        try {
            s = (String) is.readObject();
        }
        catch (Exception e){    // catch a general exception
            this.closeSocket();
            return false;
        }
        System.out.println("01. <- Received a String object from the client (" + s + ").");

        // At this point there is a valid String object
        // invoke the appropriate function based on the command
        // Là il vérifier la commande. ce qu’on peut faire, c’est d’ajouter une condition ici, pour notre command.
        if (s.equalsIgnoreCase("GetDate")){
            this.getDate();
        }
        else if(s.equalsIgnoreCase("GetTemperature")) {
            this.getTemperature();
        }
        else {
            this.sendError("Invalid command: " + s);
        }
        return true;
    }


    // Use our custom server.DateTimeService Class to get the date and time
    private void getDate() { // use the date service to get the date
        String currentDateTimeText = theDateService.getDateAndTime();
        this.send(currentDateTimeText);
    }

    private void getTemperature() { // use the date service to get the date
        readAnalog();
        double currentTemperature = temperature;
        this.send(currentTemperature);
    }


    private double convertTemperature(double adc_value) {
//        double cur_voltage = adc_value * (1.80f/4096.0f);
//        double diff_degreesC = (cur_voltage-0.75f)/0.01f;
//        return (25.0f + diff_degreesC);
        return adc_value / (double) 1000;
    }

    private void readAnalog(){
        try{
            BufferedReader bR = new BufferedReader(new FileReader(TEMP_PATH2));
            String value = bR.readLine();
            System.out.println("Value is: " + value);
            bR.close();
            //objectToSend.setTemperature(convertTemperature(Double.valueOf(value).intValue()));
            temperature = Double.parseDouble(value);
            temperature = convertTemperature(temperature);
            System.out.println("model.Temperature is : " + temperature);
        }
        catch(IOException e){
            System.out.println("Failed access");
        }
    }
    // Send a generic object back to the client 
    private void send(Object o) {
        try {
            System.out.println("02. -> Sending (" + o +") to the client.");
            this.os.writeObject(o);
            this.os.flush();
        }
        catch (Exception e) {
            System.out.println("XX." + e.getStackTrace());
        }
    }

    // Send a pre-formatted error message to the client 
    public void sendError(String message) {
        this.send("Error:" + message); //remember a String IS-A Object!
    }

    // Close the client socket 
    public void closeSocket() { //gracefully close the socket connection
        try {
            this.os.close();
            this.is.close();
            this.clientSocket.close();
        }
        catch (Exception e) {
            System.out.println("XX. " + e.getStackTrace());
        }
    }
}